#Guess The Number
import random
guess = 0

number = random.randint(1, 100)
print("Please guess my number between 1 and 100")
print("")
answer = int(input("Enter Number: "))
guess+=1

while number != answer:
    if number < answer:
        print("")
        print("Guess is too high")
        print("Please guess my number between 1 and 100")
        print("")
        answer = int(input("Enter Number: "))
        guess+=1
    elif number > answer:
        print("")
        print("Guess is too low")
        print("Please guess my number between 1 and 100")
        print("")
        answer = int(input("Enter Number: "))
        guess+=1

print("")
print("You Guessed correct in "+ str(guess) + " guesses!")
